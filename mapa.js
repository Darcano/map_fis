//Se crea el mapa
let map = L.map("map").setView([4.639386, -74.082412], 6);

//Agregar tileLayer al mapa
L.tileLayer("https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png", {
  attribution:
    '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
}).addTo(map);

let ubicaciones = [];
let categorias = [];

/*********************************************************************************************** */
console.log(
  "Ubicaciones LocalStorage",
  JSON.parse(localStorage.getItem("ubicaciones"))
);
console.log(
  "Categorias LocalStorage",
  JSON.parse(localStorage.getItem("categorias"))
);
/*********************************************************************************************** */

//Funciones para agregar las ubicaciones del localStorage
if (!localStorage.getItem("categorias")) {
  //Se definneen algunas categorias
  categorias = ["Parque", "Restaurante", "Universidad"];
  localStorage.setItem("categorias", JSON.stringify(categorias));
} else {
  categorias = JSON.parse(localStorage.getItem("categorias"));
}

if (!localStorage.getItem("ubicaciones")) {
  //Se definen algunas ubicaciones por default
  ubicaciones = [
    {
      id: 1,
      nombre: "Cuatro Parques",
      ubicacion: [4.629612, -74.065715],
      categoria: categorias[0],
      marcador: null,
    },
    {
      id: 2,
      nombre: "Dulces Delicias",
      ubicacion: [4.627848, -74.06615],
      categoria: categorias[1],
      marcador: null,
    },
  ];
  localStorage.setItem("ubicaciones", JSON.stringify(ubicaciones));
} else {
  ubicaciones = JSON.parse(localStorage.getItem("ubicaciones"));
}

//Funcion para agregar las categorias al modal
function agregarCategoriasSelect() {
  const selectCategorias = document.getElementById("select_categoria");
  while (selectCategorias.firstChild) {
    selectCategorias.removeChild(selectCategorias.firstChild);
  }

  categorias.forEach((element, index) => {
    const select = document.createElement("option");
    select.innerText = element;
    select.value = index;

    selectCategorias.appendChild(select);
  });
}

//Funcion para buscar ubicacion
function getUbicacion(id) {
  let auxUbicacion;
  ubicaciones.forEach((element) => {
    if (element.id == id) {
      auxUbicacion = element;
    }
  });
  return auxUbicacion;
}

//funcion para agregar una ubicacion a la lista
function agregarUbicacionLista(elementoUbicacion) {
  const contenedorLista = document.getElementById("contenedor_lista");
  const ubicacion = document.createElement("div");
  ubicacion.id = "ubicacion_lista_" + elementoUbicacion.id;
  ubicacion.setAttribute("class", "ubicacion_lista");
  const nombre = document.createElement("div");
  nombre.innerHTML = elementoUbicacion.nombre;

  const categoria = document.createElement("div");
  categoria.innerHTML = "Categoria: " + elementoUbicacion.categoria;

  const eliminar = document.createElement("button");
  eliminar.id = "boton_eliminar_" + elementoUbicacion.id;
  eliminar.innerText = "Eliminar";
  eliminar.setAttribute("onclick", "eliminarUbicacionLista(id)");
  eliminar.setAttribute("class", "boton_eliminar");

  const dirigir = document.createElement("button");
  dirigir.id = "boton_dirigir_" + elementoUbicacion.id;
  dirigir.innerText = "Dirigir";
  dirigir.setAttribute("onclick", "dirigirUbicacionLista(id)");
  dirigir.setAttribute("class", "boton_dirigir");

  ubicacion.appendChild(nombre);
  ubicacion.appendChild(categoria);
  ubicacion.appendChild(eliminar);
  ubicacion.appendChild(dirigir);

  contenedorLista.appendChild(ubicacion);
}

//funcion para eliminar una ubicacion de la lista
function eliminarUbicacionLista(id_boton) {
  let id = id_boton.split("_")[2];
  let auxUbicacion = getUbicacion(parseInt(id));

  /*********************************************************************************************** */
  console.log("aux: ", auxUbicacion);
  /*********************************************************************************************** */
  auxUbicacion.marcador.remove();

  document.getElementById("ubicacion_lista_" + id).remove();

  let idexDelete = 0;
  ubicaciones.forEach((element, index) => {
    if ((element.id = auxUbicacion.id)) {
      idexDelete = index;
    }
  });

  ubicaciones.splice(idexDelete, 1);

  let auxLocalUbicaciones = [];
  ubicaciones.forEach((element) => {
    auxLocalUbicaciones.push(JSON.parse(JSON.stringify(element)));
  });

  auxLocalUbicaciones.forEach((element) => {
    element.marcador = null;
  });
  localStorage.setItem("ubicaciones", JSON.stringify(auxLocalUbicaciones));
}

//funcion para dirigir el mapa a cierta ubicacion
function dirigirUbicacionLista(id_boton) {
  let id = id_boton.split("_")[2];
  let auxUbicacion = getUbicacion(parseInt(id));

  window.scrollBy(0, window.innerHeight);
  /*********************************************************************************************** */
  console.log("aux Dirigir: ", auxUbicacion);
  /*********************************************************************************************** */
  map.flyTo(auxUbicacion.ubicacion, 18);

  /********************************************************************************************************************************************************** */
  console.log("Dirigir:", ubicaciones);
  /********************************************************************************************************************************************************** */
}

//Funcion para agregar los marcadores al mapa
ubicaciones.forEach((element) => {
  //Se agrega un marcador de la ubicación en el mapa
  element.marcador = L.marker(element.ubicacion, { alt: element.id })
    .addTo(map)
    .bindPopup(
      "Ubicación: " + element.nombre + ". Categoria: " + element.categoria
    );

  //Se crea la lista de las ubicaciones
  agregarUbicacionLista(element);
  /********************************************************************************************************************************************************** */
  console.log("Se crean las ubicaciones");
  /********************************************************************************************************************************************************** */
});

//Funcion que obtine el evento al hacer click dentro del mapa
function onMapClick(e) {
  console.log(e.latlng);

  if (
    window.confirm(
      "Desea agregar la siguiente ubicación: [" +
        e.latlng.lat +
        "," +
        e.latlng.lng +
        "]"
    )
  ) {
    agregarCategoriasSelect();
    document.getElementById("modal_footer").innerText =
      "Ubicación a guardar: [" + e.latlng.lat + "," + e.latlng.lng + "]";
    localStorage.setItem("ubicacionTemporal", JSON.stringify(e.latlng));
    document.getElementById("modal1").classList.add(isVisible);
  }
}

//Se agrega la funcion a los eventos del mapa
map.on("click", onMapClick);

//Funcion para guardar una ubicacion
function guardarUbicacion() {
  console.log("Se intento agregar una ubicación");
  const categoriaUbicacion = document.getElementById("select_categoria").value;
  const nombreUbicacion = document.getElementById("input_area").value;
  const descripcionUbicacion = document.getElementById("text_area").value;
  /********************************************************************************************************************************************************** */
  console.log(
    "Categorias:",
    categoriaUbicacion,
    " nombre:",
    nombreUbicacion,
    " descripcion:",
    descripcionUbicacion
  );
  /********************************************************************************************************************************************************** */

  if (categoriaUbicacion && nombreUbicacion && descripcionUbicacion) {
    let arrayId = [];
    ubicaciones.forEach((element) => {
      arrayId.push(element.id);
    });
    let cont = 1;
    while (arrayId.indexOf(cont) != -1) {
      cont++;
    }
    let auxUbicacion = JSON.parse(localStorage.getItem("ubicacionTemporal"));
    localStorage.removeItem("ubicacionTemporal");

    let ubicacionAGuardar = {
      id: cont,
      nombre: nombreUbicacion,
      ubicacion: [auxUbicacion.lat, auxUbicacion.lng],
      categoria: categorias[parseInt(categoriaUbicacion)],
      marcador: null,
    };

    ubicacionAGuardar.marcador = L.marker(ubicacionAGuardar.ubicacion, {
      alt: ubicacionAGuardar.id,
    })
      .addTo(map)
      .bindPopup(
        "Ubicación: " +
          ubicacionAGuardar.nombre +
          ". Categoria: " +
          ubicacionAGuardar.categoria
      );

    ubicaciones.push(ubicacionAGuardar);

    agregarUbicacionLista(ubicacionAGuardar);

    let auxLocalUbicaciones = [];
    ubicaciones.forEach((element) => {
      auxLocalUbicaciones.push(JSON.parse(JSON.stringify(element)));
    });

    auxLocalUbicaciones.forEach((element) => {
      element.marcador = null;
    });
    localStorage.setItem("ubicaciones", JSON.stringify(auxLocalUbicaciones));
    document.querySelector(".modal.is-visible").classList.remove(isVisible);
  }
}

//Funciones para el modal
const openEls = document.querySelectorAll("[data-open]");
const isVisible = "is-visible";

for (const el of openEls) {
  el.addEventListener("click", function () {
    const modalId = this.dataset.open;
    document.getElementById(modalId).classList.add(isVisible);
  });
}

const closeEls = document.querySelectorAll("[data-close]");

for (const el of closeEls) {
  el.addEventListener("click", function () {
    this.parentElement.parentElement.parentElement.classList.remove(isVisible);
  });
}

document.addEventListener("click", (e) => {
  if (e.target == document.querySelector(".modal.is-visible")) {
    document.querySelector(".modal.is-visible").classList.remove(isVisible);
  }
});

document.addEventListener("keyup", (e) => {
  if (e.key == "Escape" && document.querySelector(".modal.is-visible")) {
    document.querySelector(".modal.is-visible").classList.remove(isVisible);
  }
});
